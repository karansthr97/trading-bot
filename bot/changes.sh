#!/bin/bash
read -p "Enter Your Zipline loader.py directory: "  dir1
read -p "Enter Your Zipline benchmarks.py directory: "  dir2

sed -i -e 's/data = get_benchmark_returns(symbol)/data = get_benchmark_returns(symbol, first_date, last_date)/' $dir1

echo "import pandas as pd" > $dir2
echo "from trading_calendars import get_calendar" >> $dir2
echo "def get_benchmark_returns(symbol, first_date, last_date):" >> $dir2
echo "    cal = get_calendar('NYSE')" >> $dir2
echo "    dates = cal.sessions_in_range(first_date, last_date)" >> $dir2
echo "    data = pd.DataFrame(0.0, index=dates, columns=['close'])" >> $dir2
echo "    data = data['close']" >> $dir2
echo "    return data.sort_index().iloc[1:]" >> $dir2

#/Users/karololszanski/anaconda3/envs/trade-bot/lib/python3.5/site-packages/zipline/data/benchmarks.py
#/Users/karololszanski/anaconda3/envs/trade-bot/lib/python3.5/site-packages/zipline/data/loader.py
