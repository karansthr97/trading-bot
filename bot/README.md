# Trading Bot

Project created for Python laboratories

## Installation
Set up python environment with:

```terminal
conda env create -f environment.yml
```

To activate environment use:

```terminal
conda info --envs
conda activate trade-bot
```
